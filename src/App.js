import "tailwindcss/dist/base.css";
import "styles/globalStyles.css";
import React from "react";
import Healthbuddy from "components/chatbot/Healthbuddy"
import AnimationRevealPage from "helpers/AnimationRevealPage.js";
import { Container,Content2Xl} from "components/misc/Layouts";
import Hero from "components/hero/HeroHospitalcare.js";
import Treatments from "components/treatments/Treatments";
import Services from "components/features/Services";
import HealthbuddyFeatures from "components/features/HealthbuddyFeatures.js";
import OurDoctors from "components/cards/OurDoctors.js";
import Specialites from "components/specialites/Specialites.js";
import Blog from "components/blogs/LatestBlogs.js";
import Testimonial from "components/testimonials/PatientSpeaks.js";
import FAQ from "components/faqs/FaqHospitalcare.js";
import MakeAnAppointment from "components/forms/MakeAnAppointment.js";
import SubscribeNewsLetter from "components/forms/SubscribeNewsletter.js";
import Footer from "components/footers/Footer.js";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";


export default function App() {
  return (
    <Router>
      <Switch>
        <Route path="/">
        <AnimationRevealPage>
          <Container tw="bg-gray-100 -mx-8 -mt-8 pt-8 px-8">
          <Content2Xl>
          <Hero />
          <OurDoctors/>
          <HealthbuddyFeatures/>
          <Services/>
          <Treatments/>
          <Specialites/>
          <Testimonial/>
          <MakeAnAppointment/>
          <Blog/>
          <FAQ/>
          <SubscribeNewsLetter/>
          <Footer/>
          <Healthbuddy/>
      </Content2Xl>
      </Container>
      </AnimationRevealPage>
        </Route>
      </Switch>
    </Router>
  );
}

