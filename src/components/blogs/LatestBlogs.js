import React from "react";
import { Container, ContentWithPaddingXl } from "components/misc/Layouts";
import tw from "twin.macro";
import styled from "styled-components";
import { css } from "styled-components/macro";
import { SectionHeading, Subheading as SubheadingBase } from "components/misc/Headings";
import { SectionDescription } from "components/misc/Typography";
import { ReactComponent as SvgDotPatternIcon } from "images/dot-pattern.svg";

const HeadingContainer = tw.div`text-center`;
const Subheading = tw(SubheadingBase)`mb-4`;
const Heading = tw(SectionHeading)``;
const Description = tw(SectionDescription)`mx-auto`;

const Posts = tw.div`mt-12 flex flex-wrap -mr-3 relative`;
const Post = tw.a`flex flex-col h-full bg-gray-200 rounded`;
const PostImage = styled.div`
  ${props => css`background-image: url("${props.imageSrc}");`}
  ${tw`h-64 sm:h-80 bg-center bg-cover rounded-t`}
`;
const PostText = tw.div`flex-1 px-6 py-8` 
const PostTitle = tw.h6`font-bold group-hocus:text-primary-500 transition duration-300 `;
const PostDescription = tw.p``;
const AuthorInfo = tw.div`flex`;
const AuthorImage = tw.img`w-12 h-12 rounded-full mr-3`;
const AuthorTextInfo = tw.div`text-xs text-gray-600`;
const AuthorName = tw.div`font-semibold mt-2`;
const AuthorProfile = tw.div`pt-1 font-medium`;

const PostContainer = styled.div`
  ${tw`relative z-20 mt-10 sm:pt-3 pr-3 w-full sm:w-1/2 lg:w-1/3 max-w-sm mx-auto sm:max-w-none sm:mx-0`}

  ${props => props.featured && css`
    ${tw`w-full sm:w-full lg:w-2/3`}
    ${Post} {
      ${tw`sm:flex-row items-center sm:pr-3`}
    }
    ${PostImage} {
      ${tw`sm:h-80 sm:min-h-full w-full sm:w-1/2 rounded-t sm:rounded-t-none sm:rounded-l`}
    }
    ${PostText} {
      ${tw`pl-8 pr-5`}
    }
    ${PostTitle} {
      ${tw`text-2xl`}
    }
    ${PostDescription} {
      ${tw`mt-4 text-sm font-semibold text-gray-600 leading-relaxed`}
    }
    ${AuthorInfo} {
      ${tw`mt-8 flex items-center`}
    }
    ${AuthorName} {
      ${tw`mt-0 font-bold text-gray-700 text-sm`}
    }
  `}
`;

const DecoratorBlob1 = tw(SvgDotPatternIcon)`absolute bottom-0 left-0 w-32 h-32 mb-3 ml-3 transform -translate-x-1/2 translate-y-1/2 fill-current text-gray-500 opacity-50`
const DecoratorBlob2 = tw(SvgDotPatternIcon)`absolute top-0 right-0 w-32 h-32 mt-16 mr-6 transform translate-x-1/2 -translate-y-1/2 fill-current text-gray-500 opacity-50`

export default ({
  subheading = "",
  heading = "Latest Blogs",
  description = "",
  posts = [
    {
      postImageSrc:
        "https://cdn.pixabay.com/photo/2016/02/22/11/20/vaccination-1215279_960_720.jpg",
      authorImageSrc:
        "https://images.unsplash.com/photo-1531427186611-ecfd6d936c79?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2.95&w=512&h=512&q=80",
      title: "Who is Eligible for Receiving Monoclonal Antibody Therapy for COVID-19?",
      description:"Monoclonal antibody treatment is a novel method of treating COVID-19 in people who have tested positive with the virus and are at high risk of developing severe illness. The purpose of this treatment is to lower viral loads, reduce hospitalizations, and minimize symptom severity.Similar to antibodies which are proteins that the body naturally produces to defend itself against disease, Monoclonal Antibodies are artificially created in the lab, tailor-made to fight the disease they treat. Casirivimab and Imdevimab are monoclonal antibodies that are specifically directed against the spike protein of SARS-CoV-2, designed to block the virus attachment and entry into human cells.",
      authorName: "Adam Cuppy",
      authorProfile: "Doctor",
      url: "https://hospitalcare.co.in",
      featured: true
    },
    {
      postImageSrc:
        "https://cdn.pixabay.com/photo/2019/08/23/07/53/umbrella-4425160_960_720.jpg",
      title: "Monsoon Illnesses in India typically experiences its annual Monsoons between the months of July to September",
      description:"India typically experiences its annual Monsoons between the months of July to September. As refreshing as it may feel, the onset of the rains bring with them a host of diseases and infections that can pose a serious range of health threats for you and your family.The good news, however, is that staying healthy during these months can be as simple as taking the right precautionary measures at the right time. Read more to know how you can stay safe and healthy this monsoon.",
      authorName: "Aaron Patterson",
      url: "https://hospitalcare.co.in",
      featured: false
    },
    {
      postImageSrc:"https://cdn.pixabay.com/photo/2021/08/19/09/18/woman-6557552_960_720.jpg",
      title: "Skin Care during winters",
      authorName: "Sam Phipphen",
      url: "https://hospitalcare.co.in"
    },
    {
      postImageSrc:"https://cdn.pixabay.com/photo/2020/04/08/07/19/eye-care-5016057_960_720.jpg",
      title: "Holistic Cataract Care – All You Need to Know",
      authorName: "Tony Hawk",
      url: "https://hospitalcare.co.in"
    },
    {
      postImageSrc:
        "https://images.unsplash.com/photo-1605044157421-e24bc9e131dd?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1285&q=80",
      title: "COVID-19 Home Isolation Guidelines for Mild/Asymptomatic Cases",
      authorName: "Himali Turn",
      url: "https://hospitalcare.co.in"
    }
  ]
}) => {
  return (
    <Container>
      <ContentWithPaddingXl>
        <HeadingContainer>
          {subheading && <Subheading>{subheading}</Subheading>}
          {heading && <Heading>{heading}</Heading>}
          {description && <Description>{description}</Description>}
        </HeadingContainer>
        <Posts>
          {posts.map((post, index) => (
            <PostContainer featured={post.featured} key={index}>
              <Post className="group" href={post.url}>
                <PostImage imageSrc={post.postImageSrc} />
                <PostText>
                  <PostTitle>{post.title}</PostTitle>
                  {post.featured && <PostDescription>{post.description}</PostDescription>}
                  <AuthorInfo>
                    {post.featured && <AuthorImage src={post.authorImageSrc} />}
                    <AuthorTextInfo>
                      <AuthorName>{post.authorName}</AuthorName>
                      {post.featured && <AuthorProfile>{post.authorProfile}</AuthorProfile>}
                    </AuthorTextInfo>
                  </AuthorInfo>
                </PostText>
              </Post>
            </PostContainer>
          ))}
          <DecoratorBlob1 />
          <DecoratorBlob2 />
        </Posts>
      </ContentWithPaddingXl>
    </Container>
  );
};
