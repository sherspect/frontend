import GreetingMsg from './GreetingMsg';
import HealthbuddyMsg from './HealthbuddyMsg';
import ReviewInitial from 'components/symptom_checker/ReviewInitial';
import InitialInterview from 'components/symptom_checker/InitialInterview';

export const  ChatbotSteps=[
    {
      id: 'GreetingMsg',
      component: <GreetingMsg />,
      asMessage: true,
      trigger: 'GreetingOptions',
    },
    {
      id: 'GreetingOptions',
      options: [
        { value: 1, label: 'A-Z Health Symptoms', trigger: 'AtoZSymptoms' },
        { value: 2, label: 'Healthbuddy', trigger: 'userMsg' },
        { value: 3, label: 'Symptom Checker', trigger: 'InitialQuestionName' },
      ]
    },
    {
      id: 'userMsg',
      user: true,
      trigger: 'HealthbuddyMsg',
    },
    {
      id: 'HealthbuddyMsg',
      component: <HealthbuddyMsg />,
      asMessage: true,
      trigger: 'userMsg',
    },    
    {
      id: 'AtoZSymptoms',
      component:  (
        <div style={{ maxWidth: 400, height: 400}}>
          <iframe src="https://api-bridge.azurewebsites.net/conditions/?p=all&aspect=name,overview_short,symptoms_short,symptoms_long,treatments_overview_short,other_treatments_long,self_care_long,prevention_short,causes_short&tab=1&uid=f422d3b0-7ebb-11ec-b459-b9f701b89af4" 
          title="NHS website - health a-z" 
          style={{height: '100%', width: '100%', border: '2px solid #015eb8'}} />
          </div>

      ),
      trigger: 'userMsg',
      
    },
    {
      id: 'InitialQuestionName',
      message: 'What is your name?',
      trigger: 'name',
    },
    {
      id: 'name',
      user: true,
      trigger: 'InitialQuestionGender',
    },
    {
      id: 'InitialQuestionGender',
      message: 'Hi {previousValue}! What is your gender?',
      trigger: 'gender',
    },
    {
      id: 'gender',
      options: [
        { value: 'male', label: 'Male', trigger: 'InitialQuestionAge' },
        { value: 'female', label: 'Female', trigger: 'InitialQuestionAge' },
      ],
    },
    {
      id: 'InitialQuestionAge',
      message: 'How old are you?',
      trigger: 'age',
    },
    {
      id: 'age',
      user: true,
      trigger: '7',
      validator: (value) => {
        if (isNaN(value)) {
          return 'value must be a number';
        } else if (value < 0) {
          return 'value must be positive';
        } else if (value > 120) {
          return `${value}? Come on!`;
        }

        return true;
      },
    },
    {
      id: '7',
      message: 'Great! Check out your summary',
      trigger: 'review',
    },
    {
      id: 'review',
      component: <ReviewInitial />,
      asMessage: true,
      trigger: 'update',
    },
    {
      id: 'update',
      message: 'Would you like to update some field?',
      trigger: 'update-question',
    },
    {
      id: 'update-question',
      options: [
        { value: 'yes', label: 'Yes', trigger: 'update-yes' },
        { value: 'no', label: 'No', trigger: 'end-initial' },
      ],
    },
    {
      id: 'update-yes',
      message: 'What field would you like to update?',
      trigger: 'update-fields',
    },
    {
      id: 'update-fields',
      options: [
        { value: 'name', label: 'Name', trigger: 'update-name' },
        { value: 'gender', label: 'Gender', trigger: 'update-gender' },
        { value: 'age', label: 'Age', trigger: 'update-age' },
      ],
    },
    {
      id: 'update-name',
      update: 'name',
      trigger: '7',
    },
    {
      id: 'update-gender',
      update: 'gender',
      trigger: '7',
    },
    {
      id: 'update-age',
      update: 'age',
      trigger: '7',
    },
    {
      id: 'end-initial',
      component: <InitialInterview />,
      asMessage: true,
      trigger: 'userMsg',
    },

  ];
