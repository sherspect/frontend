import React, { useState, useEffect } from "react";
import axios from "axios";
const baseURL = "https://api.hospitalcare.co.in/greet_user";

export default () => {
    const [posts, setPosts] = useState([]);
    const fetchPost = async () => {
    const response = await axios(baseURL)
        setPosts(response.data)
    }
    useEffect(() => {
      fetchPost();
     }, []);

    return (
        <>{posts.message} </>
    );
  };
  

