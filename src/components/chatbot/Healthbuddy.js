import React, { Component } from 'react';
import BotAvatar from "bot.png";
import ChatBot from 'react-simple-chatbot';
import { ThemeProvider } from 'styled-components';
import { ChatbotSteps } from './ChatbotSteps';
// all available theme props
const theme = {
    background: '#f5f8fb',
    fontFamily: 'Helvetica Neue',
    headerBgColor: '#3f37c9',
    headerFontColor: '#fff',
    headerFontSize: '15px',
    botBubbleColor: '#3f37c9',
    botFontColor: '#fff',
    userBubbleColor: '#fff',
    userFontColor: '#4a4a4a',
  };
// config for bot
const HealthBuddyConfig ={
    width: "400px",
    height: "500px",
    floating: true,
  };
class Healthbuddy extends Component {

    render() {
        return (
          <ThemeProvider theme={theme}>
            <ChatBot
          headerTitle={"Health Buddy"}
          userAvatar={BotAvatar}
          botAvatar={BotAvatar}
          recognitionEnable={true}
          steps={ChatbotSteps}
          {...HealthBuddyConfig}
          />
            </ThemeProvider>
            );
          }
        }    
  
  export default Healthbuddy;