import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Loading } from 'react-simple-chatbot';
import axios from "axios";
const baseURL = "https://api.hospitalcare.co.in/healthbuddy";
class HealthbuddyMsg extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      result: ''
    };
  }

  componentDidMount () {
    const { steps } = this.props;
    // console.log(steps);
    const userMsg = steps.userMsg.value;
    const userObject = {
        message:userMsg
      };
      axios.post(baseURL, userObject)
      .then(res => {
        //   console.log(res);
        this.setState({ loading: false, result: res.data.response });
      }).catch(function(error) {
        console.log(error);
      });

   
  }

  render() {
    const {  loading, result } = this.state;
    return (
      <div >
        { loading ? <Loading /> : result }
      </div>
    );
  }
}

HealthbuddyMsg.propTypes = {
  steps: PropTypes.object,
};

HealthbuddyMsg.defaultProps = {
  steps: undefined,
};

export default HealthbuddyMsg;