import React from "react";
import tw from "twin.macro";
import styled from "styled-components";
import { css } from "styled-components/macro"; //eslint-disable-line
import { SectionHeading, Subheading as SubheadingBase } from "components/misc/Headings.js";
import { SectionDescription } from "components/misc/Typography.js";
import { Container as ContainerBase, ContentWithPaddingXl as ContentBase } from "components/misc/Layouts.js";

const Container = tw(ContainerBase)`bg-primary-900 text-gray-100 -mx-8 px-8`;
const ContentWithPaddingXl = tw(
  ContentBase
)`relative z-10 mx-auto px-0 py-10 sm:px-6 md:px-8 lg:px-12 xl:px-24 sm:py-20 flex flex-col max-w-screen-xl`;
const HeaderContainer = tw.div`mt-10 w-full flex flex-col items-center`;
const Subheading = tw(SubheadingBase)`mb-4 text-gray-100`;
const Heading = tw(SectionHeading)`w-full`;
const Description = tw(SectionDescription)`w-full text-gray-300 text-center`;
const BotImg = styled.div`
img {${tw`h-64 w-64`}}`;
const FeaturesContainer = tw.div`mt-16 flex flex-col items-center lg:flex-row lg:items-stretch lg:justify-between text-gray-900 font-medium`;
const Features = styled.div`
  ${tw`w-full max-w-sm bg-white rounded-lg shadow-sm py-10 px-6 sm:px-10 lg:px-6 lg:py-10 xl:p-10 mx-3 flex flex-col justify-between mt-16 first:mt-0 lg:mt-0 shadow-raised`}
`;

const FeatureHeader = styled.div`
  .featuredText {
    ${tw`text-xs font-bold px-3 rounded py-2 uppercase bg-green-300 text-green-900 leading-none mt-4 sm:mt-0 w-full sm:w-auto text-center`}
  }
  .description {
    ${tw`mt-8 font-medium text-gray-700 lg:text-sm xl:text-base`}
  }
`;

const WhiteBackgroundOverlay = tw.div`absolute inset-x-0 bottom-0 h-1/6 lg:h-1/3 bg-white z-0`;

export default ({
  subheading = "",
  heading = "HealthBuddy",
  description = "Your medical companion",
  features = null,
  primaryButtonText = "HealthBuddy"
}) => {
  const defaultFeatures = [
    {
      description: "Your personal healthcare assistant for all your medical queries.",
      url: "https://hospitalcare.co.in/bot-images/bot1.jpg"
    },
    {
      description: " An dedicated Symptom checker for your diagnosis. (Powered by: Infermedica)",  
      url: "https://hospitalcare.co.in/bot-images/bot3.jpeg"},
    {
    description: "Now you can book your appointment with the help of healthbuddy. And many more.",
    url: "https://hospitalcare.co.in/bot-images/bot4.jpeg"
    }
  ];

  if (!features) features = defaultFeatures;

  return (
    <Container>
      <ContentWithPaddingXl>
        <HeaderContainer>
          {subheading && <Subheading>{subheading}</Subheading>}
          <Heading>{heading}</Heading>
          {description && <Description>{description}</Description>}
        </HeaderContainer>
        <FeaturesContainer>
          {features.map((feature, index) => (
            <Features key={index} featured={feature.featured}>
              <FeatureHeader>
                <BotImg><img src={feature.url} alt="Healthbuddy" /></BotImg>
                <p className="description">{feature.description}</p>
              </FeatureHeader>
            </Features>
          ))}
        </FeaturesContainer>
      </ContentWithPaddingXl>
      <WhiteBackgroundOverlay />
    </Container>
  );
};
