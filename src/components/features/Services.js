import React from "react";
import styled from "styled-components";
import tw from "twin.macro";
//eslint-disable-next-line
import { css } from "styled-components/macro";
import { SectionHeading } from "components/misc/Headings.js";

import defaultCardImage from "../../images/shield-icon.svg";

import { ReactComponent as SvgDecoratorBlob3 } from "../../images/svg-decorator-blob-3.svg";

import SupportIconImage from "../../images/support-icon.svg";
import ShieldIconImage from "../../images/shield-icon.svg";
import CustomizeIconImage from "../../images/customize-icon.svg";
import FastIconImage from "../../images/fast-icon.svg";
import ReliableIconImage from "../../images/reliable-icon.svg";
import SimpleIconImage from "../../images/simple-icon.svg";

const Container = tw.div`relative`;

const ThreeColumnContainer = styled.div`
  ${tw`flex flex-col items-center md:items-stretch md:flex-row flex-wrap md:justify-center max-w-screen-xl mx-auto py-20 md:py-24`}
`;
const Heading = tw(SectionHeading)`w-full`;

const Column = styled.div`
  ${tw`md:w-1/2 lg:w-1/3 px-6 flex`}
`;

const Card = styled.div`
  ${tw`flex flex-col mx-auto max-w-xs items-center px-6 py-10 border-2 border-dashed border-primary-500 rounded-lg mt-12`}
  .imageContainer {
    ${tw`border-2 border-primary-500 text-center rounded-full p-6 flex-shrink-0 relative`}
    img {
      ${tw`w-8 h-8`}
    }
  }

  .textContainer {
    ${tw`mt-6 text-center`}
  }

  .title {
    ${tw`mt-2 font-bold text-xl leading-none text-primary-500`}
  }

  .description {
    ${tw`mt-3 font-semibold text-secondary-100 text-sm leading-loose`}
  }
`;

const DecoratorBlob = styled(SvgDecoratorBlob3)`
  ${tw`pointer-events-none absolute right-0 bottom-0 w-64 opacity-25 transform translate-x-32 translate-y-48 `}
`;

export default () => {
  /*
   * This componets has an array of object denoting the cards defined below. Each object in the cards array can have the key (Change it according to your need, you can also add more objects to have more cards in this feature component):
   *  1) imageSrc - the image shown at the top of the card
   *  2) title - the title of the card
   *  3) description - the description of the card
   *  If a key for a particular card is not provided, a default value is used
   */

  const cards = [
    {
      imageSrc: ShieldIconImage,
      title: "Emergency & Trauma Care",
      description: "When some one experiences a serious injury or requires emergency surgery, the first thing you do is call an ambulance. But getting to the hospital is not all thats essential to ensure the patients survival and recovery."
    },
    { imageSrc: SupportIconImage, 
      title: "Critical Care Units", 
      description:"The team of Critical Care unit at Hospital Care constitutes a devoted team of anaesthesiology and surgical intensivists, critical care nurse practitioners, and respiratory therapists. "
     },
    { imageSrc: CustomizeIconImage, 
      title: "eCLINIC-Telemedicine Services",
      description:"With eCLINIC, carrying your physical records are a thing of the past. Every time you visit us online, your prescriptions, reports, and other health records are saved."
     },
    { imageSrc: ReliableIconImage, 
      title: "Patient Support Services",
      description:"We understand that hospitalisation can be a stressful experience, not just for the patient but also for the family and friends. At Hospital Care we pay attention to the small things that can help make your hospital stay a little bit easier."
     },
    { imageSrc: FastIconImage,
     title: "Medicine Delivery",
     description:"Buy 100% genuine medicines and get them delivered to your doorstep.Ordering is easy. Share your prescription through WhatsApp; our trained pharmacist will call you to confirm your order, and your order is dispatched." 
    },
    { imageSrc: SimpleIconImage,
     title: "Lab Tests & Diagnostics",
     description:"Diagnostic procedure is an examination to identify an individual's specific areas of weakness in order to determine a disease using external technology like CAT Scan, MRI, ECG etc.Lab test uses samples of your blood, urine or body tissue etc."
     }
  ];

  return (
    <Container>
      <ThreeColumnContainer>
        <Heading><span tw="text-primary-500">Services</span></Heading>
        {cards.map((card, i) => (
          <Column key={i}>
            <Card>
              <span className="imageContainer">
                <img src={card.imageSrc || defaultCardImage} alt="" />
              </span>
              <span className="textContainer">
                <span className="title">{card.title || "Fully Secure"}</span>
                <p className="description">
                  {card.description || "Lorem ipsum donor amet siti ceali ut enim ad minim veniam, quis nostrud. Sic Semper Tyrannis. Neoas Calie artel."}
                </p>
              </span>
            </Card>
          </Column>
        ))}
      </ThreeColumnContainer>
      <DecoratorBlob />
    </Container>
  );
};
