import React from "react";
import tw from "twin.macro";
import styled from "styled-components";
import {css} from "styled-components/macro"; //eslint-disable-line
import { SectionHeading as HeadingTitle, Subheading } from "components/misc/Headings.js";
import { PrimaryButton as PrimaryButtonBase } from "components/misc/Buttons.js";
import { ReactComponent as UserIcon } from "feather-icons/dist/icons/user.svg";
import { ReactComponent as TagIcon } from "feather-icons/dist/icons/tag.svg";
import { ReactComponent as SvgDecoratorBlob1 } from "../../images/svg-decorator-blob-1.svg";
import { ReactComponent as SvgDecoratorBlob2 } from "../../images/svg-decorator-blob-3.svg";

const Container = tw.div`relative`;
const Content = tw.div`max-w-screen-xl mx-auto py-20 lg:py-24`;
const ThreeColumn = tw.div`flex flex-col items-center lg:items-stretch lg:flex-row flex-wrap`;
const Column = tw.div`mt-24 lg:w-1/3`;

const HeadingInfoContainer = tw.div`flex flex-col items-center`;
const HeadingDescription = tw.p`mt-4 font-medium text-gray-600 text-center max-w-sm`;

const Card = tw.div`lg:mx-4 xl:mx-8 max-w-sm flex flex-col h-full`;
const Image = styled.div(props => [
  `background-image: url("${props.imageSrc}");`,
  tw`bg-cover bg-center h-80 lg:h-64 rounded rounded-b-none`
]);

const Details = tw.div`p-6 rounded border-2 border-t-0 rounded-t-none border-dashed border-primary-100 flex-1 flex flex-col items-center text-center lg:block lg:text-left`;
const MetaContainer = tw.div`flex items-center`;
const Meta = styled.div`
  ${tw`text-secondary-100 font-medium text-sm flex items-center leading-none mr-6 last:mr-0`}
  svg {
    ${tw`w-4 h-4 mr-1`}
  }
`;

const Title = tw.h5`mt-4 leading-snug font-bold text-lg`;
const Description = tw.p`mt-2 text-sm text-secondary-100`;
const Link = styled(PrimaryButtonBase).attrs({as: "a"})`
  ${tw`inline-block mt-4 text-sm font-semibold`}
`

const DecoratorBlob1 = tw(
  SvgDecoratorBlob1
)`-z-10 absolute bottom-0 right-0 w-48 h-48 transform translate-x-40 -translate-y-8 opacity-25`;
const DecoratorBlob2 = tw(
  SvgDecoratorBlob2
)`-z-10 absolute top-0 left-0 w-48 h-48 transform -translate-x-32 translate-y-full opacity-25`;

export default ({
  subheading ="",
  heading = <><span tw="text-primary-500">Specialites</span></>,
  description = "We invest our efforts, time, and expertise to detect and treat diseases",

}) => {
  const specialities = [
    {
      imageSrc:"https://cdn.pixabay.com/photo/2020/12/08/16/56/brain-5814964_960_720.jpg",
      author: "Dr Adam Wathan",
      category: "Hospital Care",
      title: "Institute of Neurosciences",
      description: "Institute of Neurosciences is an integrated institute with a dedicated team of doctors supported by the latest technology which aims to provide comprehensive and multidisciplinary care for disorders of the brain and spine. The institute has a team of highly qualified neurologists, neurosurgeons, neurointerventionists, neuro-anaesthetists, and dedicated neuro critical care specialists, neuropsychologist and neuropsychiatrists. The institute has dedicated centres for brain tumours, spinal disorders, stroke, epilepsy, movement disorders and headache. The institute has specialised services and protocols for emergency management of acute neurological disorders including stroke and other neurological trauma.",
      url: "https://hospitalcare.co.in"
    },
    {
      imageSrc:"https://cdn.pixabay.com/photo/2013/02/09/04/15/doctor-79579_960_720.jpg",
      author: "Dr Owais Khan",
      category: "Hospital Care",
      title: "Institute of Liver Transplantation and Regenerative Medicine",
      description: "Institute of Liver Transplantation and Regenerative Medicine is Asia’s first of its kind dedicated Institute offering liver transplantation and all other levels of treatment for liver and biliary diseases including cancer, both in adults and children. The Living Donor Liver Transplant program is the largest in India and second largest in the world with a team of highly qualified doctors with a rich experience of more than 2,500 Liver Transplants, having groomed the programme from its inception. The program has achieved the highest success rates (95%) and lowest infection rates (<10%) in Liver Transplant. The Institute is also a high-volume referral centre for all types of liver tumours, bile duct cancer, cysts and",
      url: "https://hospitalcare.co.in"
    },
    {
      imageSrc:
        "https://cdn.pixabay.com/photo/2020/04/16/15/39/medical-5051148_960_720.jpg",
      author: "Dr Steve Schoger",
      category: "Hospital Care",
      title: "Institute of Critical Care and Anaesthesiology",
      description: "Institute of Critical Care and Anaesthesiology constitutes a devoted team of critical care doctors who deal with the most seriously ill patients. The facility is equipped with state of the art technology and has a capacity of 300 ICU beds. Our multi-disciplinary doctors are equipped to handle all kinds of medical eventualities, including multiple organ failures and severe trauma injuries. The Institute follows global protocols for infection control which makes Hospitalcare’s infection rates comparable to the best hospitals in the world. Our highly trained anaesthesiologists provide safe, consistent and continuous pre-operative care to sick patients undergoing complex cardiac, neuro, ortho, transplant, trauma, cosmetic and minimally invasive surgeries in our 39 state-of-the-art operation theatres.",
      url: "https://hospitalcare.co.in"
    }
  ];
  return (
    <Container>
      <Content>
        <HeadingInfoContainer>
          {subheading && <Subheading>{subheading}</Subheading>}
          <HeadingTitle>{heading}</HeadingTitle>
          <HeadingDescription>{description}</HeadingDescription>
        </HeadingInfoContainer>
        <ThreeColumn>
          {specialities.map((speciality, index) => (
            <Column key={index}>
              <Card>
                <Image imageSrc={speciality.imageSrc} />
                <Details>
                  <MetaContainer>
                    <Meta>
                      <UserIcon />
                      <div>{speciality.author}</div>
                    </Meta>
                    <Meta>
                      <TagIcon />
                      <div>{speciality.category}</div>
                    </Meta>
                  </MetaContainer>
                  <Title>{speciality.title}</Title>
                  <Description>{speciality.description}</Description>
                  <Link href={speciality.url}>Know More</Link>
                </Details>
              </Card>
            </Column>
          ))}
        </ThreeColumn>
      </Content>
      <DecoratorBlob1 />
      <DecoratorBlob2 />
    </Container>
  );
};
