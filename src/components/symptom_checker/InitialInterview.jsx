import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Loading } from 'react-simple-chatbot';
import axios from "axios";
const baseURL = "https://api.hospitalcare.co.in/initial";
class InitialInterview extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      InterviewID:''
    };
  }

  componentDidMount () {
    const { steps } = this.props;
    // console.log(steps);
    const sex = steps.gender.value;
    const age = steps.age.value;
    const age_unit = "year";
    const userObject = {
        sex:sex,
        age:age,
        age_unit:age_unit
      };
      axios.post(baseURL, userObject)
      .then(res => {
        // console.log(res);
        localStorage.setItem('InterviewID', res.data);
        this.setState({ loading: false, InterviewID: res.data});
      }).catch(function(error) {
        console.log(error);
      });
  }

  render() {
    const {  loading, result } = this.state;
    return (
      <div >
        { loading ? <Loading /> : result }
      </div>
    );
  }
}

InitialInterview.propTypes = {
  steps: PropTypes.object,
};

InitialInterview.defaultProps = {
  steps: undefined,
};

export default InitialInterview;