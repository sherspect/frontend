import React from "react";
import styled from "styled-components";
import tw from "twin.macro";
import { ReactComponent as SvgDotPatternIcon } from "../../images/dot-pattern.svg";
import { SectionHeading as HeadingTitle } from "../misc/Headings.js";

const Container = tw.div`relative`;

const SingleColumn = tw.div`max-w-screen-xl mx-auto py-20 lg:py-24`;

const HeadingInfoContainer = tw.div`flex flex-col items-center`;
const HeadingDescription = tw.p`mt-4 font-medium text-gray-600 text-center max-w-sm`;

const Content = tw.div`mt-16`;

const Card = styled.div(props => [
  tw`mt-24 md:flex justify-center items-center`,
  props.reversed ? tw`flex-row-reverse` : "flex-row"
]);
const Image = styled.div(props => [
  `background-image: url("${props.imageSrc}");`,
  tw`rounded md:w-1/2 lg:w-5/12 xl:w-1/3 flex-shrink-0 h-80 md:h-144 bg-cover bg-center mx-4 sm:mx-8 md:mx-4 lg:mx-8`
]);
const Details = tw.div`mt-4 md:mt-0 md:max-w-md mx-4 sm:mx-8 md:mx-4 lg:mx-8`;
const Subtitle = tw.div`font-bold tracking-wide text-secondary-100`;
const Title = tw.h4`text-3xl font-bold text-gray-900`;
const Description = tw.p`mt-2 text-sm leading-loose`;
const Link = tw.a`inline-block mt-4 text-sm text-primary-500 font-bold cursor-pointer transition duration-300 border-b-2 border-transparent hover:border-primary-500`;

const SvgDotPattern1 = tw(
  SvgDotPatternIcon
)`absolute top-0 left-0 transform -translate-x-20 rotate-90 translate-y-8 -z-10 opacity-25 text-primary-500 fill-current w-24`;
const SvgDotPattern2 = tw(
  SvgDotPatternIcon
)`absolute top-0 right-0 transform translate-x-20 rotate-45 translate-y-24 -z-10 opacity-25 text-primary-500 fill-current w-24`;
const SvgDotPattern3 = tw(
  SvgDotPatternIcon
)`absolute bottom-0 left-0 transform -translate-x-20 rotate-45 -translate-y-8 -z-10 opacity-25 text-primary-500 fill-current w-24`;
const SvgDotPattern4 = tw(
  SvgDotPatternIcon
)`absolute bottom-0 right-0 transform translate-x-20 rotate-90 -translate-y-24 -z-10 opacity-25 text-primary-500 fill-current w-24`;

export default () => {
  const cards = [
    {
      imageSrc:
        "https://cdn.pixabay.com/photo/2018/05/24/10/16/diabetes-3426247_960_720.jpg",
      subtitle: "",
      title: "Diabetes",
      description:
        "Diabetes refers to an illness that influences how your body utilises the blood sugar known as glucose. The sugar is an important source of energy for the cells that make up your muscles and tissues. It is also vital for your brain as glucose is the main source of energy for the brain. Diabetes is known as an ailment of the pancreas (an organ behind your stomach that creates the hormone insulin). If a person is diabetic, the pancreas either cannot produce enough insulin or utilise the insulin properly. Insulin works with glucose in the circulation system to help it enter the body's cells to be burned for energy. If you are diabetic, regardless of what sort, it implies you have excess glucose in your blood which can lead to medical issues.",
      url: "https://hospitalcare.co.in"
    },

    {
      imageSrc:"https://cdn.pixabay.com/photo/2014/11/02/09/15/man-513529_960_720.jpg",
      subtitle: "",
      title: "Hypertension",
      description: "High blood pressure can cause the arteries that supply blood and oxygen to the brain to burst or be blocked, causing a stroke. Brain cells die during a stroke because they do not get enough oxygen. Stroke can cause serious disabilities in speech, movement, and other basic activities. A stroke can also kill you.Hypertension  ̶  or elevated blood pressure  ̶  is a serious medical condition that significantly increases the risks of heart, brain, kidney and other diseases.An estimated 1.28 billion adults aged 30-79 years worldwide have hypertension, most (two-thirds) living in low- and middle-income countriesAn estimated 46% of adults with hypertension are unaware that they have the condition.Less than half of adults (42%) with hypertension are diagnosed and treated.Approximately 1 in 5 adults (21%) with hypertension have it under control.Hypertension is a major cause of premature death worldwide.One of the global targets for noncommunicable diseases is to reduce the prevalence of hypertension by 33% between 2010 and 2030.",
      url: "https://hospitalcare.co.in"
    },

    {
      imageSrc:"https://cdn.pixabay.com/photo/2016/11/21/15/45/man-1846050_960_720.jpg",
      subtitle:"",
      title: "Coronary Artery Disease",
      description:"Coronary arteries are the major blood vessels that carry blood, oxygen, and nutrients to the heart. Coronary artery disease develops when your coronary artery gets damaged or afflicted. The building up of plaques causes the arteries to become narrow, thereby decreasing the flow of blood to the heart. Gradually, the decrease in the flow of blood may result in angina or chest pain, shortness of breath, and other symptoms. If there is a complete blockage, it may result in a heart attack.Coronary artery disease gets developed over decades, and, therefore, can go unnoticed until the last phase of heart attack. Timely medication and preventive actions contribute a great deal. You could start by committing to a healthy lifestyle.",
      url:"https://hospitalcare.co.in"
    }
  ];

  return (
    <Container>
      <SingleColumn>
        <HeadingInfoContainer>
          <HeadingTitle>Treatments</HeadingTitle>
          <HeadingDescription>
          A Destination for Advanced Care.
          </HeadingDescription>
        </HeadingInfoContainer>

        <Content>
          {cards.map((card, i) => (
            <Card key={i} reversed={i % 2 === 1}>
              <Image imageSrc={card.imageSrc} />
              <Details>
                <Subtitle>{card.subtitle}</Subtitle>
                <Title>{card.title}</Title>
                <Description>{card.description}</Description>
                <Link href={card.url}>See more...</Link>
              </Details>
            </Card>
          ))}
        </Content>
      </SingleColumn>
      <SvgDotPattern1 />
      <SvgDotPattern2 />
      <SvgDotPattern3 />
      <SvgDotPattern4 />
    </Container>
  );
};
